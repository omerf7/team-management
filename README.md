# Basketball Team Management Application
Application is basketball player registration for a basketball team.
## How to use ?
This is a spring boot application that build with maven. You can build and run application by issuing the following command on your terminal.  

`./mvnw spring-boot:run`

This will expose GraphQL API at "http://localhost:8080/graphql". You can consume making requests with GraphQL specific syntax.   
Also you can interact with user interface to consume api. Which is exposed at "http://localhost:8080/graphiql". It is recommended because it is beautiful and really cool.


## Technology Stack
- Java 1.8
- Maven
- Spring Boot
- Spring Data
- MySQL
- GraphQL
- GraphiQL
- Lombok
- H2

