package com.example.teammanagement.graphql;

import com.example.teammanagement.model.Position;
import com.example.teammanagement.service.PlayerDataService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MutationTest {

    @Mock
    private PlayerDataService playerDataService;
    private Mutation underTest;

    @BeforeEach
    void setUp() {
        underTest = new Mutation(playerDataService);
    }

    @Test
    void addPlayer() {
        //given
        String name = "name";
        String surname = "surname";
        //when
        underTest.addPlayer(name,surname, Position.POINT_GUARD);
        //then
        verify(playerDataService).savePlayer(name,surname,Position.POINT_GUARD);
    }

    @Test
    void deletePlayer() {
        //given
        long idForTest = 1L;
        //when
        underTest.deletePlayer(idForTest);
        //then
        verify(playerDataService).deletePlayer(idForTest);
    }
}
