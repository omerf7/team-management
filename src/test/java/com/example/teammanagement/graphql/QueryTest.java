package com.example.teammanagement.graphql;

import com.example.teammanagement.service.PlayerDataService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class QueryTest {

    @Mock
    private PlayerDataService playerDataService;
    private Query underTest;

    @BeforeEach
    void setUp() {
        underTest = new Query(playerDataService);
    }

    @Test
    void getAllPlayers() {
        //when
        underTest.getAllPlayers();
        //then
        verify(playerDataService).getAllPlayers();
    }
}
