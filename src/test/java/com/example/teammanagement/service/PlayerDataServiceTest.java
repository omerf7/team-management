package com.example.teammanagement.service;

import com.example.teammanagement.exception.PlayerNotFoundException;
import com.example.teammanagement.exception.TeamHasFullCapacityException;
import com.example.teammanagement.model.Player;
import com.example.teammanagement.model.Position;
import com.example.teammanagement.repository.PlayerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class PlayerDataServiceTest {

    @Mock
    private PlayerRepository repository;
    private PlayerDataService underTest;

    @BeforeEach
    void setUp() {
        underTest = new PlayerDataService(repository);
    }

    @Test
    void isTeamFullShouldCallCount() {
        //when
        underTest.isTeamFull();
        //then
        verify(repository).count();
    }

    @Test
    void isTeamFullShouldReturnTrueWhenCountBiggerThanTwenty() {
        //given
        when(repository.count()).thenReturn(20L);
        //when
        boolean expected = underTest.isTeamFull();
        //then
        assertThat(expected).isTrue();
    }

    @Test
    void shouldGetAllPlayers() {
        //when
        underTest.getAllPlayers();
        //then
        verify(repository).findAll();
    }

    @Test
    void canSavePlayer() {
        //given
        String name = "name";
        String surname = "surname";
        Position position = Position.CENTER;

        ArgumentCaptor<Player> playerArgumentCaptor = ArgumentCaptor.forClass(Player.class);

        //when
        underTest.savePlayer(name,surname,position);

        //then
        verify(repository).save(playerArgumentCaptor.capture());
        Player playerArgumentCaptorValue = playerArgumentCaptor.getValue();
        assertThat(playerArgumentCaptorValue.getName()).isEqualTo(name);
        assertThat(playerArgumentCaptorValue.getSurname()).isEqualTo(surname);
        assertThat(playerArgumentCaptorValue.getPosition()).isEqualTo(position);
    }
    @Test
    void savePlayerShouldThrownExceptionWhenTeamIsFull(){
        //given
        when(repository.count()).thenReturn(20L);

        //when
        //then
        assertThrows(TeamHasFullCapacityException.class,()->
                underTest.savePlayer("name", "surname", Position.CENTER));

        verify(repository,never()).save(any());
    }

    @Test
    void deletePlayerWhenPlayerExists() {
        //given
        when(repository.existsById(any())).thenReturn(true);
        //when
        boolean expected = underTest.deletePlayer(1L);
        //then
        verify(repository).deleteById(any());
        assertThat(expected).isTrue();
    }

    @Test
    void deletePlayerShouldThrownExceptionWhenPlayerDoesNotExists(){
        //given
        when(repository.existsById(any())).thenReturn(false);
        //then
        assertThrows(PlayerNotFoundException.class,
                ()-> underTest.deletePlayer(1L));
    }

    @Test
    void createPlayer() {
        //given
        String name = "name";
        String surname = "surname";
        Position position = Position.CENTER;
        //when
        assertThat(underTest.createPlayer(name,surname,position)).hasNoNullFieldsOrPropertiesExcept("id");
    }
}
