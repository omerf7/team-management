package com.example.teammanagement.integration;

import com.example.teammanagement.TeamManagementApplication;
import com.graphql.spring.boot.test.GraphQLResponse;
import com.graphql.spring.boot.test.GraphQLTestTemplate;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.jdbc.Sql;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.skyscreamer.jsonassert.JSONAssert.assertEquals;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = TeamManagementApplication.class)
@Sql(scripts = "/clean_table.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class IntegrationTest {

    @Autowired
    private GraphQLTestTemplate graphQLTestTemplate;


    @Test
    void addPlayer_ShouldReturnNonNullId() throws IOException {
        //when
        GraphQLResponse graphQLResponse = graphQLTestTemplate.postForResource("request/addPlayer_withNonNull_mutation.graphql");
        //then
        assertThat(graphQLResponse.isOk()).isTrue();
        assertThat(graphQLResponse.get("$.data.addPlayer.id")).isNotNull();
    }

    @Test
    void addPlayer_ShouldReturnExpectedResponse() throws IOException, JSONException {
        //given
        String expectedBody = read("response/addPlayer_response.json");
        //when
        GraphQLResponse graphQLResponse = graphQLTestTemplate.postForResource("request/addPlayer_mutation.graphql");
        //then
        assertThat(graphQLResponse.isOk()).isTrue();
        assertEquals(expectedBody, graphQLResponse.getRawResponse().getBody(), true);
    }

    @Test
    @Sql("/insert_data.sql")
    void deletePlayer_ShouldReturnExpectedBody() throws IOException, JSONException {
        //given
        String expectedBody = read("response/deletePlayer_response.json");
        //when
        GraphQLResponse graphQLResponse = graphQLTestTemplate.postForResource("request/deletePlayer_mutation.graphql");
        //then
        assertThat(graphQLResponse.isOk()).isTrue();
        assertEquals(expectedBody, graphQLResponse.getRawResponse().getBody(), true);
    }

    @Test
    @Sql("/insert_data.sql")
    void getAllPlayers_ShouldReturnExpectedBody() throws IOException, JSONException {
        //given
        String expectedBody = read("response/getAllPlayers_response.json");
        //when
        GraphQLResponse graphQLResponse = graphQLTestTemplate.postForResource("request/getAllPlayers_query.graphql");
        //then
        assertThat(graphQLResponse.isOk()).isTrue();
        assertEquals(expectedBody, graphQLResponse.getRawResponse().getBody(), true);
    }

    private String read(String location) throws IOException {
        return IOUtils.
                toString(new ClassPathResource(location).getInputStream(), StandardCharsets.UTF_8);
    }


}
