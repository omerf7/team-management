package com.example.teammanagement.model;


public enum Position {
    CENTER("C"),
    POINT_GUARD("PG"),
    POWER_FORWARD("PF"),
    SHOOTING_GUARD("SG"),
    SMALL_FORWARD("SF");

    /**
     * abbrev for position name
     */
    private final String code;

    public String getCode() {
        return code;
    }

    private Position(String code) {
        this.code=code;
    }
}
