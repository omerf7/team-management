package com.example.teammanagement.exception;

import com.fasterxml.jackson.annotation.JsonIgnore;
import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.GraphQLException;
import graphql.language.SourceLocation;

import java.util.List;

public class TeamHasFullCapacityException extends RuntimeException implements GraphQLError {

    public TeamHasFullCapacityException(String message) {
        super(message);
    }

    /**
     * to ignore the linked exception during serialization. This way, the stack trace won’t reach the client.
     *
     * @return StackTraceElement[]
     */
    @Override
    @JsonIgnore
    public StackTraceElement[] getStackTrace() {
        return super.getStackTrace();
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

    @Override
    public List<SourceLocation> getLocations() {
        return null;
    }

    @Override
    public ErrorType getErrorType() {
        return ErrorType.OperationNotSupported;
    }
}
