package com.example.teammanagement.exception;

import com.fasterxml.jackson.annotation.JsonIgnore;
import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.language.SourceLocation;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class PlayerNotFoundException extends RuntimeException implements GraphQLError {

    private final long id;

    public PlayerNotFoundException(String message,Long id) {
        super(message);
        this.id=id;
    }

    /**
     * to ignore the linked exception during serialization. This way, the stack trace won’t reach the client.
     * @return StackTraceElement[]
     */
    @Override
    @JsonIgnore
    public StackTraceElement[] getStackTrace() {
        return super.getStackTrace();
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

    @Override
    public Map<String, Object> getExtensions() {
        return Collections.singletonMap("invalidId", id);
    }

    @Override
    public List<SourceLocation> getLocations() {
        return null;
    }

    @Override
    public ErrorType getErrorType() {
        return ErrorType.DataFetchingException;
    }


}
