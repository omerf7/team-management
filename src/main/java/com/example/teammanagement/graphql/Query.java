package com.example.teammanagement.graphql;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.example.teammanagement.model.Player;
import com.example.teammanagement.service.PlayerDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Query implements GraphQLQueryResolver {

    private final PlayerDataService playerDataService;

    @Autowired
    public Query(PlayerDataService playerDataService) {
        this.playerDataService = playerDataService;
    }

    public List<Player> getAllPlayers(){
        return playerDataService.getAllPlayers();
    }
}
