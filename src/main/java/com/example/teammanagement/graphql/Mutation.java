package com.example.teammanagement.graphql;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.example.teammanagement.model.Player;
import com.example.teammanagement.model.Position;
import com.example.teammanagement.service.PlayerDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Mutation implements GraphQLMutationResolver {

    private final PlayerDataService playerDataService;

    @Autowired
    public Mutation(PlayerDataService playerDataService) {
        this.playerDataService = playerDataService;
    }

    public Player addPlayer(String name, String surname, Position position){
        return playerDataService.savePlayer(name, surname, position);
    }

    public boolean deletePlayer(Long id){
        return playerDataService.deletePlayer(id);
    }
}
