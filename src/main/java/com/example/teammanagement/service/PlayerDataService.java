package com.example.teammanagement.service;

import com.example.teammanagement.exception.PlayerNotFoundException;
import com.example.teammanagement.exception.TeamHasFullCapacityException;
import com.example.teammanagement.model.Player;
import com.example.teammanagement.model.Position;
import com.example.teammanagement.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * this class performs CRUD operations with help of JPA repository
 */
@Component
public class PlayerDataService {

    private final PlayerRepository playerRepository;

    @Autowired
    public PlayerDataService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    /**
     * determine if the team has full capacity.in this case we can
     * have 20 players in a team.
     *
     * @return true if rows bigger than 20 in table.
     */
    public boolean isTeamFull() {
        long count = playerRepository.count();
        return count >= 20;
    }

    public List<Player> getAllPlayers() {
        return playerRepository.findAll();
    }

    /**
     * save the player to the team and return that object.
     * if team is full throw TeamHasFullCapacityException
     *
     * @param name     player name
     * @param surname  player surname
     * @param position player position
     * @return Player or TeamHasFullCapacityException
     */
    public Player savePlayer(String name, String surname, Position position) {
        Player player = createPlayer(name, surname, position);
        if (isTeamFull()) {
            throw new TeamHasFullCapacityException
                    ("Team can have maximum twenty players and it has it's full capacity.You can't add another one.");
        } else {
            return playerRepository.save(player);
        }
    }

    public boolean deletePlayer(long id) {
        if (playerRepository.existsById(id)) {
            playerRepository.deleteById(id);
            return true;
        } else {
            throw new PlayerNotFoundException("Player not found with given id: " + id, id);
        }
    }

    public Player createPlayer(String name, String surname, Position position) {
        Player player = new Player();
        player.setName(name);
        player.setSurname(surname);
        player.setPosition(position);
        return player;
    }


}
